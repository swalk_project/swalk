# SWALK - Safe crossWALKs in urban areas: assessment of countermeasures to improve pedestrian safety.

Multi-year jointed project of the Departmen of Civil and Environmental Engineering (DICEA) and the Department of Neurosciences, Psychology, Drug Research and Child Health (NEUROFARBA) of the University of studies of Florence (Italy).

## Goal of the project
The SWALK Project is aimed to enhance the understanding of the factors affecting accidents involving pedestrians and to contribute to the effort to reduce fatalities and injuries by improving the compatibility between the pedestrian behaviour and the infrastructures.
In order to assess the effectiveness of the proposed solution both direct safety measures (reduction in the number of crashes and their severity)
as well as indirect safety measures related to pedestrian’s and driver’s behaviour will be evaluated in virtual reality where the traditional zebra crossing provided by
the Italian Road Code will be compared to modified zebra crossings that take into account improved or alternative countermeasures that helps to induce a right behaviour by the drivers.
Specifically, the experiments carried out with the help of two high-technology tools (driving simulator and eye tracker) will allow to identify the best treatment in the most critical pedestrian crossing in the local municipal area analysed in the previous point.

## Project workflow

```mermaid
graph TD
    WP1[WP1: State of the art]-->WP2[WP2: Detailed site investigations and accident analysis]
    WP1-->WP3[WP3: Driving simulator experiments]
    WP2<-->WP3
    WP3-->WP4[WP4: Dissemination and exploitation]
    

    style WP1 fill:#e0ffe0, stroke:#00cc00
    style WP2 fill:#fff3eb, stroke:#ff6600
    style WP3 fill:#fffbeb, stroke:#ffcc00
    style WP4 fill:#ebf3ff, stroke:#0066ff


```
## Content of the repository

## Contacts
Giulia Martini,  e-mail: martini.giulia94@gmail.com    
Monica Meocci, e-mail: monica.meocci@unifi.it  
Valentina Branzi, e-mail: valentina.branzi@unifi.it  
