---
layout: page
title: About
folder: About
---

## Safe crossWALKs in urban areas: assessment of countermeasures to improve pedestrian safety.

Multi-year joint project of the Department of <a href="https://www.dicea.unifi.it/changelang-eng.html">Civil and Environmental Engineering</a> (DICeA) and the Department of <a href="https://www.neurofarba.unifi.it/">Neurosciences, Psychology, Drug Research and Child Health</a> (NEUROFARBA) of the University of studies of Florence (Italy).

### Goal of the project
The S-Walk Project is aimed to enhance the understanding of the factors affecting accidents involving pedestrians and to contribute to reduce fatalities and injuries by improving the compatibility between the pedestrian behaviour and the infrastructures.
In order to assess the effectiveness of the proposed solution both direct safety measures (reduction in the number of crashes and their severity)
as well as indirect safety measures related to pedestrian’s and driver’s behaviour will be evaluated in virtual reality where the traditional zebra crossing provided by
the Italian Road Code will be compared to modified zebra crossings that take into account improved or alternative countermeasures that helps to induce a right behaviour by the drivers.
Specifically, the experiments carried out with the help of two high-technology tools (driving simulator and eye tracker) will allow to identify the best treatment in the most critical pedestrian crossing in the local municipal area analysed in the previous point.

### Project workflow
The project is divided in four main workpackages as follows:
<img SRC="{{site.url}}{{ site.baseurl }}img/workflow.PNG" alt="workflow" title="workflow" height="400"  /> 



