---
layout: page
title: People
folder: People
---

<a href="https://www.unifi.it/p-doc2-2015-0-A-2c303c323b2c-1.html">Dr Monica Meocci</a> attained a degree in Civil Engineering (2009) and a PhD in Civil and Environmental Engineering (2013) at the University of Florence. From January 2013 to May 2017 she worked as Associate Researcher at the Department of<a href="https://www.dicea.unifi.it/changelang-eng.html"> Civil and Environmental Engineering</a> (DICeA) of the University of Florence. From June 2017 to date she worked as a fixed-term Researcher at the DICeA developing and investigating different topics related to pedestrian safety in urban areas. From 2010 to date she was involved in National and European Research projects focused on road safety. The most relevant European projects were funded by CEDR call “Road Safety” and H2020 call “Mobility for Growth”. From March 2019 she is responsible of the national research project with Municipality of Fiesole concerning the definition of a specific pedestrian accident prediction model. She is the author of more than 20 scientific and technical papers. 


Dr. Meocci’s research interests are focused on the assessment of the safety effectiveness of road treatments based both on the road environmental configuration and behavioral response of the road users. Great interest is also aroused by the analysis of road materials performance in order to optimize the road configuration with reference to durability and maintenance. 


<a href="https://www.linkedin.com/in/giulimartini/">Giulia Martini</a> attained a M.Sc degree in Structural Engineering at Delft University of Technology. From December 2019 she is a researcher at the <a href="https://www.tno.nl/en/Netherlands"> Organisation for Applied Scientific Research</a> (TNO) and from May 2020 she works at the S-Walk project for the University of Florence. Her research interests are broad and range from structures health monitoring and predictive maintenance to road safety. 
She is also the mantainer of this website. So, If you do not like it. **Blame her!** 
<img src="{{site.url}}{{ site.baseurl }}img/Giuli_SA.jpg" alt="Giulia Martini" title="Giulia Martini" height="300"  /> 
