---
layout: page
title: Workshop
folder: Workshop
---

We plan to organise a workshop in Florence in the second half of 2020.
It will be also streamed...You can`t miss it!

**This page will be updated as soon as more information are available.**

<p class="message">
  Would you like to be updated on the S-Walk workshop?<br />
  Leave us your e-mail and we will get in touch with you!<br />
</p>

<form
  action="https://formspree.io/xrgybnqk"
  method="POST"
>
  <label>
    Your email:
    <input type="text" name="_replyto" style="width: 300px;">
  </label>

  <!-- your other form fields go here -->

  <button type="submit">Send</button>
</form>

<img src="{{site.url}}{{ site.baseurl }}img/Florence.jpg" alt="Giulia Martini" title="Giulia Martini" style="width:100%;"  /> 

