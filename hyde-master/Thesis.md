---
layout: page
title: Thesis
folder: Thesis
---

We are looking for interested and pro-active university students who would like to write their Bachelor or Master thesis with us.
S-Walk is a multidisciplinary project, we have many ideas and we are enthusiast to listen to your interests to find the perfect topic for you.
Below a list of some thesis topics currently available or in development.

If you are interested, <a href="{{site.url}}{{ site.baseurl }}Contacts/index.html"> contact us
      </a>!

|Thesis Topic|
|---|
|Analysis of the Italian code and of the guidelines for the design of crosswalks.|
|---|
| Data-analysis of the car accidents in the Province of Florence with focus on the chracteristics of the most dangerous crossings.|
|---|
| Study of the risk perception at crossings with different characteristics.|