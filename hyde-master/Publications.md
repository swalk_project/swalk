---
layout: page
title: Publications
folder: Publications
---

Here our publications related to S-Walk:

|Title| Author| doi|
|:---|:---|:---|
|Virtual testing of speed reduction schemes on urban collector roads.| L.Domenichini, V. Branzi, M. Meocci|<a href="https://doi.org/10.1016/j.aap.2017.09.020"> doi</a>|
|:---|:---|---|
|Virtual testing of speed reduction schemes on urban collector roads.| V. Branzi, M. Meocci, L. Domenichini, F. La Torre|---|
|---|---|---|

