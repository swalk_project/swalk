---
layout: page
title: Contact us
folder: Contacts
---

<p class="message">
  Would you like to know more?<br />
  Would you like to get involved? <br /> 
  CONTACT US!  
</p>

<form
  action="https://formspree.io/xrgybnqk"
  method="POST"
>
  <label>
    Your email:
    <input type="text" name="_replyto" style="width: 300px;">   
  </label>
  <br/>
  <label>
    Your message:
    <textarea name="message"></textarea>
  </label>

  <!-- your other form fields go here -->

  <button type="submit">Send</button>
</form>

